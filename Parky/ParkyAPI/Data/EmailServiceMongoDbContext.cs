﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace ParkyAPI.Data
{
    public class EmailServiceMongoDbContext
    {
        private readonly IMongoDatabase _database;

        public EmailServiceMongoDbContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<EmailReportItemDataStore> EmailBulkReports => _database.GetCollection<EmailReportItemDataStore>("EmailBulkReports");
    }

    public class EmailReportItemDataStore
    {
        /*
         "type": "d",
   "timeLogged": "2020-05-09 12:18:38+0200",
   "timeQueued": "2020-05-09 12:18:38+0200",
   "orig": "julieconseille@site.onlequeu-pipujaj.fr",
   "rcpt": "acaat@numericable.fr",
   "orcpt": "",
   "dsnAction": "relayed",
   "dsnStatus": "2.0.0 (success)",
   "dsnDiag": "smtp;250 2.0.0 Ok: queued as 53C961C0006EB",
   "dsnMta": "smtp-in.sfr.fr (93.17.128.123)",
   "bounceCat": "",
   "srcType": "smtp",
   "srcMta": "site.onlequeu-pipujaj.fr (52.42.167.110)",
   "dlvType": "smtp",
   "dlvSourceIp": "51.38.51.49",
   "dlvDestinationIp": "93.17.128.123",
   "dlvEsmtpAvailable": "ENHANCEDSTATUSCODES,PIPELINING,8BITMIME,SIZE,ETRN,STARTTLS",
   "dlvSize": 18909,
   "vmta": "VMTA-us-onlequeu-pipujaj.fr",
   "jobId": "",
   "envId": "",
   "queue": "numericable.fr/VMTA-us-onlequeu-pipujaj.fr",
   "vmtaPool": "",
   "header_Content-Type": "text/html; charset=utf-8",
   "header_Message-Id": "<3.6.6.9088.68BDBA5A7B6AF41.5.5EB6834B87BE1.97412.22859@82FD69FB4AACE3C71E94E755CBEBC084>",
   "header_X-Sendership-Sending-ID": "",
   "header_Return-Path": "",
   "header_Feedback-ID": "",
   "header_X-SenId": "",
   "header_X-CreaId": ""
         */

        [BsonId] public ObjectId Id { get; set; }
        [Required] public string TimeLogged { get; set; }
        [Required] public string TimeQueued { get; set; }
        [Required] public string Orig { get; set; }
        [Required] public string Rcpt { get; set; }
        public string Orcpt { get; set; }
        [Required] public string DsnAction { get; set; }
        [Required] public string DsnStatus { get; set; }
        [Required] public string DsnDiag { get; set; }
        [Required] public string DsnMta { get; set; }
        public string BounceCat { get; set; }
        [Required] public string SrcType { get; set; }
        [Required] public string SrcMta { get; set; }
        [Required] public string DlvType { get; set; }
        [Required] public string DlvSourceIp { get; set; }
        [Required] public string DlvDestinationIp { get; set; }
        [Required] public string DlvEsmtpAvailable { get; set; }
        public long DlvSize { get; set; }
        [Required] public int Vmta { get; set; }
        public Guid JobId { get; set; }
        public Guid EnvId { get; set; }
        [Required] public string Queue { get; set; }
        public string VmtaPool { get; set; }
        [BsonElement("header_Content-Type")]
        [Required] public string HeaderContentType { get; set; }
        [BsonElement("header_Message-Id")]
        [Required] public string HeaderMessageId { get; set; }
        [BsonElement("header_X-Sendership-Sending-ID")]
        public string HeaderXSendershipSendingId { get; set; }
        [BsonElement("header_Return-Path")]
        public string HeaderReturnPath { get; set; }
        [BsonElement("header_Feedback-ID")]
        public string HeaderFeedbackId { get; set; }
        [BsonElement("header_X-SenId")]
        public string HeaderXSenId { get; set; }
        [BsonElement("header_X-CreaId")]
        public string HeaderXCreaId { get; set; }
        public DateTime Created { get; set; }
    }

    public class Settings
    {
        internal readonly string ConnectionString;
        internal readonly string Database;

        public Settings()
        {

        }

        public Settings(string connString, string dbName)
        {
            ConnectionString = connString;
            Database = dbName;
        }
    }
}
