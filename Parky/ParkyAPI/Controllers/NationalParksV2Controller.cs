﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Controllers
{
    [Route("api/v{version:apiVersion}/nationalparks")]
    [ApiVersion("2.0")]
    [ApiController]
    //[ApiExplorerSettings(GroupName = "ParkyOpenAPISpecNP")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public class NationalParksV2Controller : Controller
    {
        private readonly  INationalParkRepository _nPRepo;
        private readonly IMapper _mapper;

        public NationalParksV2Controller(INationalParkRepository nPRepo, IMapper mapper)
        {
            _nPRepo = nPRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// Get list of all national parks.
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [ProducesResponseType(200,Type = typeof(List<NationalParkDto>))]
        //This return the all national parks
        public IActionResult GetNationalParks()
        {
            var obj = _nPRepo.GetNationalParks().FirstOrDefault();
            var objDto = new List<NationalParkDto>();

            return Ok(_mapper.Map<NationalParkDto>(obj)); //domain model'imizi değil dto'sunu döndürürüz. 
        }

    }
}