﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Controllers
{
    [Route("api/v{version:apiVersion}/nationalparks")]
    //[Route("api/[controller]")]
    [ApiController]
    //[ApiExplorerSettings(GroupName = "ParkyOpenAPISpecNP")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public class NationalParksController : Controller
    {
        private readonly  INationalParkRepository _nPRepo;

        private readonly IMapper _mapper;

        public NationalParksController(INationalParkRepository nPRepo, IMapper mapper)
        {
            _nPRepo = nPRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// Get list of all national parks.
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [ProducesResponseType(200,Type = typeof(List<NationalParkDto>))]
        //This return the all national parks
        public IActionResult GetNationalParks()
        {
            var objList = _nPRepo.GetNationalParks();

            var objDto = new List<NationalParkDto>();

            foreach (var obj in objList)
            {
                objDto.Add(_mapper
                    .Map<NationalParkDto>(
                        obj)); //nationalPark bizim domainimiz old için direkt onu döndüremeyiz. Bu nedenle dto'suna çevirdik ve onu döndüreceğiz.
            }

            return Ok(objDto); //domain model'imizi değil dto'sunu döndürürüz. 
        }

        /// <summary>
        /// Get individual national park.
        /// </summary>
        /// <param name="nationalParkId">The Id of the national park </param>
        /// <returns></returns>

        [HttpGet("{nationalParkId:int}",Name= "GetNationalPark")]
        [ProducesResponseType(200, Type = typeof(NationalParkDto))]
        [ProducesResponseType(404)] //Not Found
        [ProducesDefaultResponseType]
        //this return single national park,  based on id
        public IActionResult GetNationalPark(int nationalParkId)
        {
            var obj = _nPRepo.GetNationalPark(nationalParkId);
            if (obj == null)
            {
                return NotFound();

            }

            // if object is not null, that means we found the object
            var objDto = _mapper.Map<NationalParkDto>(obj); //before returning, we need to convert that into our dto 
            //var objDto = new NationalParkDto() // autoMapper olmasaydı aşağıdaki kodları yazarak yapmamız gerekirdi
            //{
            //    Created = obj.Created,
            //    Id = obj.Id,
            //    Name = obj.Name,
            //    State = obj.State,
            //};
            return Ok(objDto);

        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(NationalParkDto))]
        [ProducesResponseType(StatusCodes.Status201Created)] //created
        [ProducesResponseType(StatusCodes.Status404NotFound)] //Not Found
        [ProducesResponseType(StatusCodes.Status500InternalServerError)] 
        public IActionResult CreateNationalPark([FromBody] NationalParkDto nationalParkDto)
        {
            if (nationalParkDto == null) // boş girilirse
            {
                return BadRequest(ModelState);
                //modelState typically contains all of the errors if any are encountered
            }

            if (_nPRepo.NationalParkExists(nationalParkDto.Name)) // var olan bir kayıt girilirse
            {
                ModelState.AddModelError("","National Park Exists!");
                return StatusCode(404, ModelState);
            }

            //if (!ModelState.IsValid) // geçerli bir kayıt girilmezse
            //{
            //    return BadRequest(ModelState);
            //}

            var nationalParkObj = _mapper.Map<NationalPark>(nationalParkDto); //dto'sunu nationalPark a çeviriyoruz.

            if (!_nPRepo.CreateNationalPark(nationalParkObj))
            {
                ModelState.AddModelError("",$"Something went wrong when saving the record {nationalParkObj.Name}" );
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetNationalPark",new {nationalParkId=nationalParkObj.Id},nationalParkObj);

        }

        [HttpPatch("{nationalParkId:int}", Name = "UpdateNationalPark")]
        [ProducesResponseType(204)]
        [ProducesResponseType(StatusCodes.Status404NotFound)] //Not Found
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult UpdateNationalPark(int nationalParkId, [FromBody] NationalParkDto nationalParkDto)
        {
            if (nationalParkDto == null || nationalParkId!=nationalParkDto.Id ) // boş girilirse ya da girilen id dto idsi ile uymazsa
            {
                return BadRequest(ModelState);
                //modelState typically contains all of the errors if any are encountered
            }

            var nationalParkObj = _mapper.Map<NationalPark>(nationalParkDto); //dto'sunu nationalPark a çeviriyoruz.

            if (!_nPRepo.UpdateNationalPark(nationalParkObj))
            {
                ModelState.AddModelError("", $"Something went wrong when updating the record {nationalParkObj.Name}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
        [HttpDelete("{nationalParkId:int}", Name = "UpdateNationalPark")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]

        public ActionResult DeleteNationalPark(int nationalParkId)
        {
            if (!_nPRepo.NationalParkExists(nationalParkId))//girilen idnin var olup olmadığını kontrol ediyoruz
            {
                return NotFound();
            }

            var nationalParkObj = _nPRepo.GetNationalPark(nationalParkId);

            if (!_nPRepo.DeleteNationalPark(nationalParkObj))
            {
                ModelState.AddModelError("", $"Something went wrong when deleting the record {nationalParkObj.Name}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}