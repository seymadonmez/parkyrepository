﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Controllers
{
    //[Route("api/Trails")]
    [Route("api/v{version:apiVersion}/trails")]
    [ApiController]
    //[ApiExplorerSettings(GroupName = "ParkyOpenAPISpecTrails")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public class TrailsController : Controller
    {
        private readonly  ITrailRepository _trailRepo;

        private readonly IMapper _mapper;

        public TrailsController(ITrailRepository trailRepo, IMapper mapper)
        {
            _trailRepo = trailRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// Get list of all trails.
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [ProducesResponseType(200,Type = typeof(List<TrailDto>))]
        //This return the all national parks
        public IActionResult GetTrails()
        {
            var objList = _trailRepo.GetTrails();

            var objDto = new List<TrailDto>();

            foreach (var obj in objList)
            {
                objDto.Add(_mapper
                    .Map<TrailDto>(
                        obj)); //trail bizim domainimiz old için direkt onu döndüremeyiz. Bu nedenle dto'suna çevirdik ve onu döndüreceğiz.
            }

            return Ok(objDto); //domain model'imizi değil dto'sunu döndürürüz. 
        }

        /// <summary>
        /// Get individual trail.
        /// </summary>
        /// <param name="trailId">The Id of the trail </param>
        /// <returns></returns>

        [HttpGet("{trailId:int}",Name= "GetTrail")]
        [ProducesResponseType(200, Type = typeof(TrailDto))]
        [ProducesResponseType(404)] //Not Found
        [ProducesDefaultResponseType]
        //this return single national park,  based on id
        public IActionResult GetTrail(int trailId)
        {
            var obj = _trailRepo.GetTrail(trailId);
            if (obj == null)
            {
                return NotFound();

            }

            // if object is not null, that means we found the object
            var objDto = _mapper.Map<TrailDto>(obj); //before returning, we need to convert that into our dto 
            //var objDto = new TrailDto() // autoMapper olmasaydı aşağıdaki kodları yazarak yapmamız gerekirdi
            //{
            //    Created = obj.Created,
            //    Id = obj.Id,
            //    Name = obj.Name,
            //    State = obj.State,
            //};
            return Ok(objDto);

        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(TrailDto))]
        [ProducesResponseType(StatusCodes.Status201Created)] //created
        [ProducesResponseType(StatusCodes.Status404NotFound)] //Not Found
        [ProducesResponseType(StatusCodes.Status500InternalServerError)] 
        public IActionResult CreateTrail([FromBody] TrailCreateDto trailDto)
        {
            if (trailDto == null) // boş girilirse
            {
                return BadRequest(ModelState);
                //modelState typically contains all of the errors if any are encountered
            }

            if (_trailRepo.TrailExists(trailDto.Name)) // var olan bir kayıt girilirse
            {
                ModelState.AddModelError("","Trail Exists!");
                return StatusCode(404, ModelState);
            }

            //if (!ModelState.IsValid) // geçerli bir kayıt girilmezse
            //{
            //    return BadRequest(ModelState);
            //}

            var trailObj = _mapper.Map<Trail>(trailDto); //dto'sunu trail a çeviriyoruz.

            if (!_trailRepo.CreateTrail(trailObj))
            {
                ModelState.AddModelError("",$"Something went wrong when saving the record {trailObj.Name}" );
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetTrail",new {trailId=trailObj.Id},trailObj);//return type ı trailDto (upsert değil). çünkü yukarıda responsetype ı da trailDto verdik.

        }

        [HttpPatch("{trailId:int}", Name = "UpdateTrail")]
        [ProducesResponseType(204)]
        [ProducesResponseType(StatusCodes.Status404NotFound)] //Not Found
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult UpdateTrail(int trailId, [FromBody] TrailUpdateDto trailDto)
        {
            if (trailDto == null || trailId!=trailDto.Id ) // boş girilirse ya da girilen id dto idsi ile uymazsa
            {
                return BadRequest(ModelState);
                //modelState typically contains all of the errors if any are encountered
            }

            // aşağıda trailUpsert'i trailDto ya çeviriyoruz. 
            var trailObj = _mapper.Map<Trail>(trailDto); //dto'sunu trail a çeviriyoruz.

            if (!_trailRepo.UpdateTrail(trailObj))
            {
                ModelState.AddModelError("", $"Something went wrong when updating the record {trailObj.Name}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
        [HttpDelete("{trailId:int}", Name = "UpdateTrail")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]

        public ActionResult DeleteTrail(int trailId)
        {
            if (!_trailRepo.TrailExists(trailId))//girilen idnin var olup olmadığını kontrol ediyoruz
            {
                return NotFound();
            }

            var trailObj = _trailRepo.GetTrail(trailId);

            if (!_trailRepo.DeleteTrail(trailObj))
            {
                ModelState.AddModelError("", $"Something went wrong when deleting the record {trailObj.Name}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}