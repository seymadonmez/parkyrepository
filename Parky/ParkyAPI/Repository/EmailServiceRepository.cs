﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using ParkyAPI.Data;
using ParkyAPI.Repository.IRepository;

namespace ParkyAPI.Repository
{
    public class EmailServiceRepository: IEmailServiceRepository
    {
        private readonly EmailServiceMongoDbContext _dbContext;

        public EmailServiceRepository(IOptions<Settings> settings)
        {
            _dbContext = new EmailServiceMongoDbContext(settings);
        }

        public async Task<IAsyncCursor<EmailReportItemDataStore>> GetEmailBulkReportsByJobIdAsync(Guid jobId)
        {
            var filter = Builders<EmailReportItemDataStore>.Filter.Eq("jobId", jobId);
            var sort = Builders<EmailReportItemDataStore>.Sort.Ascending("rcpt");

            IAsyncCursor<EmailReportItemDataStore> queryResult = await _dbContext.EmailBulkReports.FindAsync(filter,
                new FindOptions<EmailReportItemDataStore, EmailReportItemDataStore>()
                {
                    Sort = sort
                });

            return queryResult;
        }

        public async Task<IAsyncCursor<EmailReportItemDataStore>> GetEmailBulkReportsByRecipientAsync(Guid jobId, string recipient)
        {
            var filter = Builders<EmailReportItemDataStore>.Filter.And(
                Builders<EmailReportItemDataStore>.Filter.Eq("jobId", jobId),
                Builders<EmailReportItemDataStore>.Filter.Eq("rcpt", recipient));
                var sort = Builders<EmailReportItemDataStore>.Sort.Ascending("rcpt");

            IAsyncCursor<EmailReportItemDataStore> queryResult = await _dbContext.EmailBulkReports.FindAsync(filter,
                new FindOptions<EmailReportItemDataStore, EmailReportItemDataStore>()
                {
                    Sort = sort
                });

            return queryResult;
        }
    }
}
