﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using ParkyAPI.Data;

namespace ParkyAPI.Repository.IRepository
{
    public interface IEmailServiceRepository
    {
        public Task<IAsyncCursor<EmailReportItemDataStore>> GetEmailBulkReportsByJobIdAsync(Guid jobId);
        public Task<IAsyncCursor<EmailReportItemDataStore>> GetEmailBulkReportsByRecipientAsync(Guid jobId, string recipient);
    }
}
