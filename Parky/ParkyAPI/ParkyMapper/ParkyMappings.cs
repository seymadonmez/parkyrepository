﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;

namespace ParkyAPI.ParkyMapper
{
    public class ParkyMappings:Profile 
    {
        public ParkyMappings() //tüm mappingleri bu constructor içinde yapıcaz
        {
            CreateMap<NationalPark, NationalParkDto>().ReverseMap(); //it will map both ways
            // it will map nationalPark to NationalParkDto. and it would also convert NatParkDto object NationalPark
            CreateMap<Trail, TrailDto>().ReverseMap();
            CreateMap<Trail, TrailUpdateDto>().ReverseMap();
            CreateMap<Trail, TrailCreateDto>().ReverseMap();

        }

    }
}
