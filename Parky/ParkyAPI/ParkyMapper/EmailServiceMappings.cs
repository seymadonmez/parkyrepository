﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MongoDB.Bson.Serialization;
using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;

namespace ParkyAPI.ParkyMapper
{
    public class EmailServiceMappings : Profile 
    {
        public EmailServiceMappings() //tüm mappingleri bu constructor içinde yapıcaz
        {
            CreateMap<EmailReportItemDataStore, EmailServiceDto>().ReverseMap(); //it will map both ways
        }

    }

}
