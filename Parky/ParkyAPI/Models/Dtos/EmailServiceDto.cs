﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ParkyAPI.Models.Dtos
{
    public class EmailServiceDto
    {
        [BsonId] public ObjectId Id { get; set; }
        [Required] public string TimeLogged { get; set; }
        [Required] public string TimeQueued { get; set; }
        [Required] public string Orig { get; set; }
        [Required] public string Rcpt { get; set; }
        public string Orcpt { get; set; }
        [Required] public string DsnAction { get; set; }
        [Required] public string DsnStatus { get; set; }
        [Required] public string DsnDiag { get; set; }
        [Required] public string DsnMta { get; set; }
        public string BounceCat { get; set; }
        [Required] public string SrcType { get; set; }
        [Required] public string SrcMta { get; set; }
        [Required] public string DlvType { get; set; }
        [Required] public string DlvSourceIp { get; set; }
        [Required] public string DlvDestinationIp { get; set; }
        [Required] public string DlvEsmtpAvailable { get; set; }
        public long DlvSize { get; set; }
        [Required] public int Vmta { get; set; }
        public Guid JobId { get; set; }
        public Guid EnvId { get; set; }
        [Required] public string Queue { get; set; }
        public string VmtaPool { get; set; }
        [BsonElement("header_Content-Type")]
        [Required] public string HeaderContentType { get; set; }
        [BsonElement("header_Message-Id")]
        [Required] public string HeaderMessageId { get; set; }
        [BsonElement("header_X-Sendership-Sending-ID")]
        public string HeaderXSendershipSendingId { get; set; }
        [BsonElement("header_Return-Path")]
        public string HeaderReturnPath { get; set; }
        [BsonElement("header_Feedback-ID")]
        public string HeaderFeedbackId { get; set; }
        [BsonElement("header_X-SenId")]
        public string HeaderXSenId { get; set; }
        [BsonElement("header_X-CreaId")]
        public string HeaderXCreaId { get; set; }
        public DateTime Created { get; set; }
    }

}
